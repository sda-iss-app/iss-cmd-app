package com.iss.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iss.app.client.IssClient;
import com.iss.app.controller.ConsoleController;
import com.iss.app.service.IssService;
import com.iss.app.ui.Menu;

public class Main {

    private static Menu MENU = new Menu();

    public static void run() {
        var issClient = new IssClient();
        var mapper = new ObjectMapper();
        var issService = new IssService(issClient, mapper);
        var consoleController = new ConsoleController(issService);
        MENU.start();
        MENU.redirect();

    }
}
