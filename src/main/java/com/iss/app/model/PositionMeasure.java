package com.iss.app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "position_measure")
public class PositionMeasure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "measured_at")
    @CreationTimestamp
    private LocalDateTime measuredAt;
//timestamp in seconds
    @Column(name = "measure_timestamp")
    private Long measureTimestamp;

    private Float latitude;

    private Float longitude;
    @OneToMany
    @JoinColumn(referencedColumnName = "iss_speed")
    @Column(name = "iss_speed_id")
    private Long issSpeedId;


}
