package com.iss.app.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "craft")
public class Craft {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<PersonInMeasurement> personInMeasurement;

    public Craft(String name) {
        this.name = name;
    }

    public Craft(String name, PersonInMeasurement personInMeasurement) {
        this.name = name;
        this.personInMeasurement = Set.of(personInMeasurement);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
