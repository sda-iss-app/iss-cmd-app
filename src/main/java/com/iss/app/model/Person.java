package com.iss.app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class Person {
    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, PersonInMeasurement personInMeasurement) {
        this.name = name;
        this.personInMeasurement = Set.of(personInMeasurement);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Craft craft;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private Set<PersonInMeasurement> personInMeasurement;

    @Override
    public String toString() {
        return "\nname = " +
                this.name +
                ", craft = " +
                this.craft;
    }
}
