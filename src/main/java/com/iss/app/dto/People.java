package com.iss.app.dto;

import com.iss.app.model.Person;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.List;

@Data
@NoArgsConstructor
public class People {
   private List<Person> people;
   private Integer number;
   private String message;
}
