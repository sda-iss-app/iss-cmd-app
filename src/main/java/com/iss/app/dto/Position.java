package com.iss.app.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.iss.app.model.IssPosition;
import lombok.Data;
import lombok.ToString;

@Data
public class Position {
private Long timestamp;
private String message;

@JsonProperty("iss_position")
private IssPosition issPosition;

public String toString() {
    return this.issPosition.toString();
}

}
