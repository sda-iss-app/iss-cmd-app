package com.iss.app.controller;

import com.iss.app.model.Person;
import com.iss.app.dto.Position;
import com.iss.app.service.IssService;

import java.util.List;

public class ConsoleController {

    private final IssService issService;


    public ConsoleController(IssService issService) {
        this.issService = issService;
    }

    public Position getCurrentPosition() {
        return issService.getPosition();
    }

    public Double getIssSpeed() {
        return 0.0;
    }

    public List<Person> getListOfPeople() {
        return issService.getAstronauts();
    }
}
