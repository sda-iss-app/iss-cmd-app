package com.iss.app.client;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class IssClient {

    private static final Logger LOGGER = Logger.getLogger(IssClient.class);

    public String get(String uri) {
        HttpRequest request = null;

        try {
           request = HttpRequest.newBuilder()
                    .uri(new URI(uri))
                    .GET()
                    .build();
        } catch (URISyntaxException e) {
            LOGGER.error("Invalid uri", e);
            return "";
        }

        try {
            HttpResponse<String> response = HttpClient
                    .newBuilder()
                    .proxy(ProxySelector.getDefault())
                    .build()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();

        } catch (IOException | InterruptedException exception) {
            LOGGER.error("Failed to fetch data from API", exception);
            return "";
        }


    }
}
