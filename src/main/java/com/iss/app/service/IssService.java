package com.iss.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iss.app.client.IssClient;
import com.iss.app.dto.People;
import com.iss.app.dto.Position;
import com.iss.app.model.Person;
import org.apache.log4j.Logger;

import java.util.List;

/*

URIs:

list of astronauts:  http://api.open-notify.org/astros.json

Iss' position: http://api.open-notify.org/iss-now.json


 */

public class IssService {

    private static final String ASTROS_URI = "http://api.open-notify.org/astros.json";
    private static final String POSITION_URI = "http://api.open-notify.org/iss-now.json";

    private final ObjectMapper mapper;
    private final IssClient issClient;
    private static final Logger LOGGER = Logger.getLogger(IssService.class);

    public IssService(IssClient issClient, ObjectMapper mapper) {
        this.issClient = issClient;
        this.mapper = mapper;
    }

    public List<Person> getAstronauts() {
        String json = issClient.get(ASTROS_URI);
        return deserializeAstronauts(json);


    }

    public Position getPosition() {
        String json = issClient.get(POSITION_URI);
        return deserializePosition(json);
    }

    private Position deserializePosition(String json) {
        try {
            return mapper.readValue(json, new TypeReference<Position>() {
            });
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to deserialize", e);
            return null;
        }
    }

    private List<Person> deserializeAstronauts(String json) {

        try {
            return mapper.readValue(json, new TypeReference<People>() {
            }).getPeople();

        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to deserialize", e);
            return List.of();
        }


    }
}
