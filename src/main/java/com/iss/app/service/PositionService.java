package com.iss.app.service;


import com.iss.app.dto.Position;
import com.iss.app.model.IssSpeed;
import com.iss.app.model.PositionMeasure;
import org.hibernate.Transaction;

import java.util.Set;

public class PositionService {

    private final HibernateService hibernateService;

    public PositionService(HibernateService hibernateService) {
        this.hibernateService = hibernateService;
    }

    public Position getById(Long id) {
        return hibernateService.getSession().get(Position.class, id);
    }

    public Position create(Position position) {
        var transaction = hibernateService.getSession().beginTransaction();

        hibernateService.getSession().persist(
                createPositionMeasure(position)
        );

        transaction.commit();

        return position;
    }

    public IssSpeed storeSpeed(Position issPosition1, Position issPosition2) {
        Transaction transaction = hibernateService.getSession().beginTransaction();

        IssSpeed issSpeed = new IssSpeed();
        issSpeed.setSpeed(0f); // TODO calculate speed
        issSpeed.setPositions(Set.of(
                createPositionMeasure(issPosition1),
                createPositionMeasure(issPosition2)
        ));

        hibernateService.getSession().persist(issSpeed);

        transaction.commit();

        return issSpeed;
    }

    private PositionMeasure createPositionMeasure(Position position) {
        var positionMeasure = new PositionMeasure();

        positionMeasure.setLatitude(position.getIssPosition().getLatitude());
        positionMeasure.setLongitude(position.getIssPosition().getLongitude());
        positionMeasure.setMeasureTimestamp(position.getTimestamp());

        return positionMeasure;
    }
}
