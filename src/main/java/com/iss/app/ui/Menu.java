package com.iss.app.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iss.app.client.IssClient;
import com.iss.app.controller.ConsoleController;
import com.iss.app.service.IssService;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class Menu {
    private static final Logger LOGGER = Logger.getLogger(Menu.class);
    private static final Scanner scanner = new Scanner(System.in);
    private static final IssService issService = new IssService(new IssClient(), new ObjectMapper());
    private static final  ConsoleController consoleController = new ConsoleController(issService);
    private static final String stretchRow = "----------------------------------";
    public void start() {

        LOGGER.info("Welcome to ISS survey application!");
        LOGGER.info(stretchRow);
        LOGGER.info("Choose one of the options below \n (insert corresponding number)");
        mainMenu();


    }

    public void mainMenu() {

        LOGGER.info(stretchRow);
        LOGGER.info("(1) - Print out ISS's position");
        LOGGER.info("(2) - Print out ISS's speed");
        LOGGER.info("(3) - Print out list of people in space");
        LOGGER.info("(0) - Exit application");
        LOGGER.info(stretchRow);
    }

    public void redirect() {
//prepared for service outputs

        while (true) {

            switch (readInput()) {
                case 1:
                    LOGGER.info("printing position...");
                    LOGGER.info(consoleController.getCurrentPosition());
                    break;
                case 2:
                    LOGGER.info("printing speed function result...");
                    LOGGER.info(consoleController.getIssSpeed());
                    break;
                case 3:
                    LOGGER.info("printing list of people in space...");
                    LOGGER.info(consoleController.getListOfPeople());
                    break;

                case 0:
                    LOGGER.info("exiting app");
                    return;
                default:
                    LOGGER.info("this option (" + readInput() + ") is not valid, please insert other value");
                    mainMenu();
                    break;
            }
        }
    }

    public Integer readInput() {
       String input = scanner.nextLine();
        try {
           return Integer.parseInt(input);

       } catch (NumberFormatException e) {
           return -1;
       }
    }
    }

